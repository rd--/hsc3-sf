# hsc3-sf

## au header

Print `NeXT/AU` header.

~~~~
$ hsc3-sf au header ll ~/sw/hsc3-sf/data/au/mc-12-4096.au
magic = .snd
data-offset/bytes = 32
data-size/bytes = 196608
data-encoding = Float
sample-rate = 1
channels = 4096
annotation =
$ hsc3-sf au header hl ~/sw/hsc3-sf/data/au/mc-12-4096.au
Header {frameCount = 12, encoding = Float, sampleRate = 1, channelCount = 4096}
$
~~~~

## au rescale

Change gain of `NeXT/AU` file.

## au stat

Print trivial `NeXT/AU` file statistics.

~~~~
$ hsc3-sf au stat plain ~/data/audio/gebet.au
file-size/bytes = 12962704
data-offset/bytes = 24
encoding = Float
encoding-size/bytes = 4
frame-count/header = 1620335
frame-count/data = 1620335
sample-rate = 44100
channel-count = 2
duration/seconds = 36.742290249433104
minima = [-0.870972752571106,-1.0]
maxima = [0.7287465929985046,0.9855019450187683]
$
~~~~

## multi-channel

Generate set of multiple-channel `NeXT/AU` or `WAVE` sound files for test purposes.

The files encode the channel number in binary.

~~~~
$ hsc3-sf multi-channel au ~/sw/hsc3-sf/data/au 2 12
$ ls -sS1 ~/sw/hsc3-sf/data/au
total 380
196 mc-12-4096.au
 92 mc-11-2048.au
 44 mc-10-1024.au
 20 mc-9-512.au
 12 mc-8-256.au
  4 mc-7-128.au
  4 mc-6-64.au
  4 mc-5-32.au
  4 mc-4-16.au
  4 mc-3-8.au
  4 mc-2-4.au
$ hsc3-au-print cs ro bw ~/sw/hsc3-sf/data/au/mc-3-8.au
nm="/home/rohan/sw/hsc3-sf/data/au/mc-3-8.au", nc=8, nf=3, sr=1, co=f
0 0 0
1 0 0
0 1 0
1 1 0
0 0 1
1 0 1
0 1 1
1 1 1
$ hsc3-sf multi-channel wave ~/sw/hsc3-sf/data/wave 2 12
$
~~~~

## riff print-ch

Print RIFF file chunk structure.

~~~~
$ hsc3-sf riff print-ch ~/sw/hsc3-data/data/akai/S5000/ARIEL/S50_AR\ C2.wav
...
$ hsc3-sf riff print-ch ~/sw/hsc3-data/data/akai/S5000/ARIEL/S50_ARIEL.akp
...
$
~~~~

## wave header

Print WAVE file header, either low-level (ll)

~~~~
$ hsc3-sf wave header ll ~/data/audio/pf-c5.wav
WAVE_FMT_16
{ audioFormat = WAVE_FORMAT_PCM
, numChannels = 1
, sampleRate = 44100
, byteRate = 88200
, blockAlign = 2
, bitsPerSample = 16
}
numFrames = 511183
'data' = 1022366,'fmt ' = 16
$
~~~~

or high-level (hl) form.

~~~~
$ hsc3-sf wave header hl ~/data/audio/pf-c5.wav
Header {frameCount = 511183, encoding = Linear16, sampleRate = 44100, channelCount = 1}
$
~~~~
