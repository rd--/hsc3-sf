# au-print

Printer for 32-bit floating point `NeXT/AU` sound files, written in ANSI C.

~~~~
$ hsc3-au-print cs co bw ~/sw/hsc3-sf/data/au/mc-4-16.au
nm="~/sw/hsc3-sf/au/mc-4-16.au", nc=16, nf=4, sr=1, co=t
0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1
0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1
0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1
0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1
$
~~~~
