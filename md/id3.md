# id3

`add-id3` reads ID3 tags from a text file and adds them to one or more MP3, M4A or FLAC files.

MP3 files are edited using `mid3v2`, M4A file using `AtomicParsley` and FLAC using `metaflac`.

The meta data text file is read until the first empty line only, and each line is parsed as either `KEY=VALUE` or `KEY: VALUE`.

~~~~
$ cd ~/uc/invisible/day/flac
$ cat 2016-03-13.7.meta
TIT2=Through the broken light of day (Organ & computer)
TALB=Through the broken light of day
TPE1=Rohan Drape
TDRC=2016-03-13
WOAR=http://rohandrape.net/
$ hsc3-id3 mp3 add-id3 2016-03-13.7.meta 2016-03-13.7.mp3
$ mid3v2 -l 2016-03-13.7.mp3
IDv2 tag info for 2016-03-13.7.mp3
TALB=Through the broken light of day
TDRC=2016-03-13
TIT2=Through the broken light of day (Organ & computer)
TPE1=Rohan Drape
WOAR=[u'http://rohandrape.net/']
$
~~~~
