-- | Vector util
module Sound.File.Vector where

import qualified Data.ByteString.Lazy as B {- bytestring -}
import qualified Data.Int as I {- base -}
import qualified Data.Vector.Storable as V {- vector -}
import System.IO {- base -}

import qualified Sound.Osc.Coding.Byte as C {- hosc -}

-- * Convert

int_to_i64 :: Int -> I.Int64
int_to_i64 = fromIntegral

i64_to_int :: I.Int64 -> Int
i64_to_int = fromIntegral

-- * bs = ByteString

-- | Section given as (start-index,element-count) duple.
bs_section :: (I.Int64, I.Int64) -> B.ByteString -> B.ByteString
bs_section (st, nb) b = B.take nb (B.drop st b)

bs_element :: Int -> B.ByteString -> Int -> B.ByteString
bs_element k b n = bs_section (int_to_i64 (n * k), int_to_i64 k) b

-- | Decode an F32 value at F32-element index /n/.
bs_index_f32 :: B.ByteString -> Int -> Float
bs_index_f32 b n = C.decode_f32 (bs_element 4 b n)

bs_index_f32_le :: B.ByteString -> Int -> Float
bs_index_f32_le b n = C.decode_f32_le (bs_element 4 b n)

bs_length :: B.ByteString -> Int
bs_length = i64_to_int . B.length

-- * vec = Vector

vec_generate_f32 :: Int -> B.ByteString -> V.Vector Float
vec_generate_f32 n b = V.generate n (bs_index_f32 b)

vec_generate_f32_le :: Int -> B.ByteString -> V.Vector Float
vec_generate_f32_le n b = V.generate n (bs_index_f32_le b)

vec_read_f32 :: Handle -> Int -> IO (V.Vector Float)
vec_read_f32 h n = fmap (vec_generate_f32 n) (B.hGet h (n * 4))

vec_write_f32 :: Handle -> Int -> V.Vector Float -> IO ()
vec_write_f32 h n v =
  let f i = B.hPut h (C.encode_f32 (v V.! i))
  in mapM_ f [0 .. n - 1]

-- | Given channel count, de-interleave vector to list of channel vectors.
vec_deinterleave :: V.Storable n => Int -> V.Vector n -> [V.Vector n]
vec_deinterleave nc v =
  let nf = V.length v `div` nc
      f c = V.generate nf (\i -> v V.! ((i * nc) + c))
  in map f [0 .. nc - 1]

-- | Interleave list of vectors.
vec_interleave :: V.Storable n => [V.Vector n] -> V.Vector n
vec_interleave l =
  let nc = length l
      nf = case l of
        [] -> error "vec_interleave: null"
        v : _ -> V.length v
      gen_f i = let (f, c) = i `divMod` nc in (l !! c) V.! f
  in V.generate (nf * nc) gen_f

{- | Search /v/ starting at /n/ for the next ascending zero crossing,
returning the left index, or 'Nothing' if there isn't one.
If /n/ is a valid index returns /n/.

>>> let v = V.fromList [-3,-1,1,3,1,-1,-3,-1,1]
>>> map (vec_next_zc v) [0,1,2]
[Just 1,Just 1,Just 7]
-}
vec_next_zc :: (Num n, Ord n, V.Storable n) => V.Vector n -> Int -> Maybe Int
vec_next_zc v n =
  let nf = V.length v
      recur x i =
        if i >= nf - 1
          then Nothing
          else
            let y = v V.! (i + 1)
            in if (x < 0) && (y >= 0)
                then Just i
                else recur y (i + 1)
  in recur (v V.! n) n

{- | Locate the next /n/ zero crossings.

>>> let v = V.fromList [-3,-1,1,3,1,-1,-3,-1,1]
>>> map (vec_next_zc_seq 2 v) [0,1,2]
[[1,7],[1,7],[7]]
-}
vec_next_zc_seq :: (Num n, Ord n, V.Storable n) => Int -> V.Vector n -> Int -> [Int]
vec_next_zc_seq n v i =
  if n == 0
    then []
    else case vec_next_zc v i of
      Nothing -> []
      Just j -> j : vec_next_zc_seq (n - 1) v (j + 1)
