-- | Read and write Next/Sun format sound files.
module Sound.File.Next (module N) where

import Sound.File.Header as N
import Sound.File.Next.Hl as N
import Sound.File.Next.Ll as N
