-- | Riff format functions.
module Sound.File.Riff where

import Data.Char {- base -}
import Data.Int {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Data.Word {- base -}
import Numeric {- base -}
import System.IO {- base -}

import qualified Data.Binary as Binary {- binary -}
import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.List.Split as Split {- split -}

import qualified Sound.Osc.Coding.Byte as Byte {- hosc -}
import qualified Sound.Osc.Coding.Convert as Convert {- hosc -}

-- * Coding

-- | Type specialised 'fromIntegral'
word32_to_int64 :: Word32 -> Int64
word32_to_int64 = fromIntegral

-- | Type-specialised 'ByteString.unpack' of 'Binary.encode'.
word32_unpack :: Word32 -> [Word8]
word32_unpack = ByteString.unpack . Binary.encode

{- | File and chunk types are stored as 4-byte Ascii, unpack from 'Word32'.

>>> word32_to_ascii 0x2e736e64
".snd"

>>> word32_to_ascii 0x52494646
"RIFF"
-}
word32_to_ascii :: Word32 -> String
word32_to_ascii = map Convert.word8_to_char . word32_unpack

{- | Inverse of 'word32_to_ascii'.

>>> ascii_to_word32 ".snd" == 0x2e736e64
True

>>> ascii_to_word32 "RIFF" == 0x52494646
True

>>> ascii_to_word32 "WAVE" == 0x57415645
True

>>> ascii_to_word32 "fmt " == 0x666d7420
True

>>> ascii_to_word32 "fact" == 0x66616374
True

>>> ascii_to_word32 "data" == 0x64617461
True
-}
ascii_to_word32 :: String -> Word32
ascii_to_word32 = Binary.decode . ByteString.pack . map Convert.char_to_word8

read_word32_ascii :: Handle -> IO String
read_word32_ascii = fmap word32_to_ascii . Byte.read_word32

write_word32_ascii :: Handle -> String -> IO ()
write_word32_ascii h = Byte.write_word32 h . ascii_to_word32

-- * Bytestring

-- | Section of ByteString, offset and size are both in bytes. Offset is from start.
section_int64 :: Int64 -> Int64 -> ByteString.ByteString -> ByteString.ByteString
section_int64 offset size =
  ByteString.take size
    . ByteString.drop offset

section_word32 :: Word32 -> Word32 -> ByteString.ByteString -> ByteString.ByteString
section_word32 offset size =
  section_int64 (Convert.word32_to_int64 offset) (Convert.word32_to_int64 size)

section_int :: Int -> Int -> ByteString.ByteString -> ByteString.ByteString
section_int offset size =
  section_int64 (Convert.int_to_int64 offset) (Convert.int_to_int64 size)

-- * List

reverse_lookup :: Eq k => k -> [(v, k)] -> Maybe v
reverse_lookup k = lookup k . map (\(p, q) -> (q, p))

-- * Pp

{- | Hex pretty-printer, upper-case, pad-left.

>>> map (show_hex_uc 2) [0x00,0x0F,0xF0]
["00","0F","F0"]
-}
show_hex_uc :: (Show n, Integral n) => Int -> n -> String
show_hex_uc n =
  let pad_left k l = replicate (n - length l) k ++ l
  in pad_left '0' . map toUpper . flip showHex ""

-- * Chunk-Hdr

-- | (Chunk-Type,Chunk-Size)
type Chunk_Hdr = (String, Word32)

chunk_hdr_pp :: Chunk_Hdr -> String
chunk_hdr_pp (ty, sz) = concat ["'", ty, "' = ", show sz]

-- * Chunk

-- | (Chunk-Header,Chunk-Data)
type Chunk = (Chunk_Hdr, ByteString.ByteString)

find_chunk :: String -> [Chunk] -> Maybe Chunk
find_chunk nm = find (\((ty, _), _) -> ty == nm)

find_chunk_err :: String -> [Chunk] -> Chunk
find_chunk_err nm = fromMaybe (error "find_chunk_err") . find_chunk nm

filter_chunks :: (String -> Bool) -> [Chunk] -> [Chunk]
filter_chunks f = filter (f . fst . fst)

-- * Parse

riff_parse_type :: ByteString.ByteString -> String
riff_parse_type = word32_to_ascii . Binary.decode

riff_parse_chunk_seq :: ByteString.ByteString -> [Chunk]
riff_parse_chunk_seq d =
  if ByteString.length d == 0
    then []
    else
      let ty = riff_parse_type (section_int64 0 4 d)
          sz = Byte.decode_word32_le (section_int64 4 4 d)
          dat = section_word32 8 sz d
      in ((ty, sz), dat) : riff_parse_chunk_seq (ByteString.drop (8 + word32_to_int64 sz) d)

-- * Io

riff_read_chunk_hdr :: Handle -> IO Chunk_Hdr
riff_read_chunk_hdr h = do
  ty <- read_word32_ascii h
  sz <- Byte.read_word32_le h
  return (ty, sz)

riff_read_chunk :: Handle -> IO Chunk
riff_read_chunk h = do
  (ty, sz) <- riff_read_chunk_hdr h
  dat <- ByteString.hGet h (Convert.word32_to_int sz)
  return ((ty, sz), dat)

riff_read_chunk_seq :: Handle -> IO [Chunk]
riff_read_chunk_seq h = do
  e <- hIsEOF h
  if e then return [] else riff_read_chunk h >>= \c -> fmap (c :) (riff_read_chunk_seq h)

riff_write_chunk_hdr :: Handle -> Chunk_Hdr -> IO ()
riff_write_chunk_hdr h (ty, sz) = do
  write_word32_ascii h ty
  Byte.write_word32_le h sz

riff_write_chunk :: Handle -> Chunk -> IO ()
riff_write_chunk h (hdr, dat) = do
  riff_write_chunk_hdr h hdr
  ByteString.hPut h dat

-- * Guid

-- | Split Guid into 4-2-2-2-8 structure.
riff_guid_segment :: [Word8] -> [[Word8]]
riff_guid_segment = Split.splitPlaces [4 :: Int, 2, 2, 2, 8]

{- | Pretty print Guid, with grouping and byte-ordering.

>>> riff_guid_pp [194,185,18,131,110,46,212,17,168,36,222,91,150,195,171,33]
"8312B9C2-2E6E-11D4-A824-DE5B96C3AB21"
-}
riff_guid_pp :: [Word8] -> String
riff_guid_pp w =
  let f op = concatMap (show_hex_uc 2) . op
      w' = riff_guid_segment w
      w'' = zipWith f (replicate 3 reverse ++ replicate 2 id) w'
  in intercalate "-" w''

-- | 'riff_guid_pp' of 'ByteString.unpack'
decode_guid :: ByteString.ByteString -> String
decode_guid = riff_guid_pp . ByteString.unpack
