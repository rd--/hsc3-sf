-- | Encode audio data.
module Sound.File.Encode where

import Data.Int {- base -}
import Data.List {- base -}

import qualified Data.Binary as Binary {- binary -}
import qualified Data.ByteString.Lazy as ByteString {- bytestring -}

import qualified Sound.Osc.Coding.Byte as Coding.Byte {- hosc -}

import qualified Sound.File.Header as Header {- hsc3-sf -}

{- | Interleave channel data, ie. 'concat' '.' 'transpose'.

>>> interleave [[0,2..8],[1,3..9]]
[0,1,2,3,4,5,6,7,8,9]
-}
interleave :: [[a]] -> [a]
interleave = concat . transpose

encode_fp :: (Fractional f, Real r) => (f -> ByteString.ByteString) -> [[r]] -> ByteString.ByteString
encode_fp f = ByteString.concat . map (f . realToFrac) . interleave

encode_f32 :: Real n => [[n]] -> ByteString.ByteString
encode_f32 = encode_fp Coding.Byte.encode_f32

encode_f32_le :: Real n => [[n]] -> ByteString.ByteString
encode_f32_le = encode_fp Coding.Byte.encode_f32_le

encode_f64 :: Real n => [[n]] -> ByteString.ByteString
encode_f64 = encode_fp Coding.Byte.encode_f64

encode_f64_le :: Real n => [[n]] -> ByteString.ByteString
encode_f64_le = encode_fp Coding.Byte.encode_f64_le

encode_i16 :: [[Int16]] -> ByteString.ByteString
encode_i16 = ByteString.concat . map Binary.encode . interleave

encode_i16_le :: [[Int16]] -> ByteString.ByteString
encode_i16_le = ByteString.concat . map Coding.Byte.encode_int16_le . interleave

-- | Given 'Encoding' and a set of channels, 'interleave' and encode as 'ByteString.ByteString'.
encode :: Real n => Header.Encoding -> [[n]] -> ByteString.ByteString
encode enc d =
  case enc of
    Header.Float -> encode_f32 d
    Header.Double -> encode_f64 d
    _ -> error "hsc3-sf: encode: not Float or Double"

encode_le :: Real n => Header.Encoding -> [[n]] -> ByteString.ByteString
encode_le enc d =
  case enc of
    Header.Float -> encode_f32_le d
    _ -> error "encode_le: not implemented"
