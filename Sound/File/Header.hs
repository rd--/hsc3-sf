-- | Very simple generic sound file header data type.
module Sound.File.Header where

-- * Encoding

-- | Enumeration of valid audio data encodings.
data Encoding
  = -- | Signed 8-bit integer
    Linear8
  | -- | Signed 16-bit integer
    Linear16
  | -- | Signed 32-bit integer
    Linear32
  | -- | IEEE 32-bit floating point
    Float
  | -- | IEEE 64-bit floating point
    Double
  deriving (Eq, Show)

-- | Bytes per sample at 'Encoding'.
encoding_bytes :: Num n => Encoding -> n
encoding_bytes e =
  case e of
    Linear8 -> 1
    Linear16 -> 2
    -- Linear24 -> 3
    Linear32 -> 4
    Float -> 4
    Double -> 8

-- * Header

-- | Sample rate at 'Header'.
type SampleRate = Int

-- | Number of frames at 'Header'.
type FrameCount = Int

-- | Number of channels at 'Header'.
type ChannelCount = Int

-- | Data type encapsulating sound file meta data.
data Sf_Header = Sf_Header
  { frameCount :: FrameCount
  , encoding :: Encoding
  , sampleRate :: SampleRate
  , channelCount :: ChannelCount
  }
  deriving (Eq, Show)

-- | Frame count divided by sample-rate.
sf_header_duration :: Fractional n => Sf_Header -> n
sf_header_duration (Sf_Header nf _ sr _) = fromIntegral nf / fromIntegral sr
