-- | Run external sound file utilities.
module Sound.File.Util where

import System.Process {- process -}

type Cmd = (String, [String])

cmd_to_str :: Cmd -> String
cmd_to_str (cmd, arg) = unwords (cmd : arg)

run_cmd_ :: Cmd -> IO ()
run_cmd_ = uncurry callProcess

-- * ffmpeg

-- | Extract section of audio file, from t1 to t2, both given in seconds.
sf_extract :: Double -> Double -> FilePath -> FilePath -> Cmd
sf_extract t1 t2 f1 f2 = ("ffmpeg", ["-ss", show t1, "-to", show t2, "-i", f1, f2])

-- * sndfile

{- | Convert and normalise sound file.

>>> cmd_to_str (sf_normalise "x.wav" "y.flac")
"sndfile-convert -normalize x.wav y.flac"
-}
sf_normalise :: FilePath -> FilePath -> Cmd
sf_normalise f1 f2 = ("sndfile-convert", ["-normalize", f1, f2])

-- * lame

-- | Allowed bit-rates for MPEG-1.
mp3_1_bit_rates :: Num n => [n]
mp3_1_bit_rates = [32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320]

-- | Encode to MP3
sf_encode_mp3 :: Int -> FilePath -> FilePath -> Cmd
sf_encode_mp3 bit_rate f1 f2 = ("lame", ["-b", show bit_rate, f1, f2])

{-

-- * EBU

-- | <https://tech.ebu.ch/loudness>, note that this resamples to 192khz.
sf_normalise_ebu :: FilePath -> FilePath -> Cmd
sf_normalise_ebu f1 f2 = ("ffmpeg",["-i",f1,"-filter:a","loudnorm",f2])

-- > cmd_to_str (sf_extract_ebu 11.35296 17.165952 "z.flac" "z.01.flac")
sf_extract_ebu :: Double -> Double -> FilePath -> FilePath -> Cmd
sf_extract_ebu t1 t2 f1 f2 = ("ffmpeg",["-ss",show t1,"-to",show t2,"-i",f1,"-filter:a","loudnorm",f2])

-}
