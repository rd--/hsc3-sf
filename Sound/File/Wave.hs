-- | Read and write Wave/Riff format sound files.
module Sound.File.Wave where

import Control.Monad {- base -}
import Data.Int {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Data.Word {- base -}
import System.IO {- base -}

import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Sound.Osc.Coding.Byte as Byte {- hosc -}
import qualified Sound.Osc.Coding.Convert as Convert {- hosc -}

import qualified Sound.File.Decode as Decode
import qualified Sound.File.Encode as Encode
import qualified Sound.File.Header as Header
import qualified Sound.File.Riff as Riff
import qualified Sound.File.Vector as Vector

-- * Fact

-- | Generate fact chunk, given number of frames (not samples).
wave_fact_ch :: Word32 -> Riff.Chunk
wave_fact_ch nf = (("fact", 4), Byte.encode_word32_le nf)

-- | The fact chunk contains the number of frames (not samples) at the data chunk.
wave_fact_parse :: Riff.Chunk -> Word32
wave_fact_parse (hdr, dat) =
  case hdr of
    ("fact", 4) -> Byte.decode_word32_le (Riff.section_word32 0 4 dat)
    _ -> error "wave_fact_parse?"

-- * Wave

-- | Read Wave chunk sequence.
wave_read_ch :: Handle -> IO [Riff.Chunk]
wave_read_ch h = do
  (ty, _) <- Riff.riff_read_chunk_hdr h
  when (ty /= "RIFF") (error "Riff.riff_read: not Riff")
  ty' <- Riff.read_word32_ascii h
  when (ty' /= "WAVE") (error "Riff.riff_read: not Wave")
  Riff.riff_read_chunk_seq h

-- | Write Wave file given sequence of chunks.
wave_write_ch :: Handle -> [Riff.Chunk] -> IO ()
wave_write_ch h ch = do
  let riff_sz = 4 + sum (map (\((_, sz), _) -> sz + 8) ch)
  Riff.riff_write_chunk_hdr h ("RIFF", riff_sz)
  Riff.write_word32_ascii h "WAVE"
  mapM_ (Riff.riff_write_chunk h) ch

-- | Format of samples.  Pcm or Float or Extensible.
data Wave_Format
  = Wave_Format_Pcm
  | Wave_Format_Ieee_Float
  | Wave_Format_Extensible
  deriving (Eq, Show)

wave_format_enum_tbl :: [(Wave_Format, Int)]
wave_format_enum_tbl =
  [ (Wave_Format_Pcm, 0x0001)
  , (Wave_Format_Ieee_Float, 0x0003)
  , (Wave_Format_Extensible, 0xFFFE)
  ]

instance Enum Wave_Format where
  fromEnum = fromJust . flip lookup wave_format_enum_tbl
  toEnum = fromJust . flip Riff.reverse_lookup wave_format_enum_tbl

-- | "fmt " chunk
data Wave_Fmt_16 = Wave_Fmt_16
  { audioFormat :: Wave_Format
  -- ^ Word16
  , numChannels :: Word16
  -- ^ Word16
  , sampleRate :: Word32
  -- ^ Word32
  , byteRate :: Word32
  -- ^ Word32
  , blockAlign :: Word16
  -- ^ Word16
  , bitsPerSample :: Word16
  -- ^ Word16
  , extraFmt :: Word16
  -- ^ Word16
  }
  deriving (Show)

wave_fmt_16_pp :: Wave_Fmt_16 -> String
wave_fmt_16_pp =
  let f c = if c `elem` ",}" then ['\n', c] else [c]
      g s = if s == "{" then "\n{ " else s
  in concatMap (g . f) . show

wave_fmt_16_encoding_tbl :: [((Wave_Format, Word16), Header.Encoding)]
wave_fmt_16_encoding_tbl =
  [ ((Wave_Format_Pcm, 8), Header.Linear8)
  , ((Wave_Format_Pcm, 16), Header.Linear16)
  , ((Wave_Format_Pcm, 32), Header.Linear32)
  , ((Wave_Format_Ieee_Float, 32), Header.Float)
  , ((Wave_Format_Ieee_Float, 64), Header.Double)
  ]

-- | Translate 'Wave_Fmt_16' encoding data to 'Header.Encoding'.
wave_fmt_16_to_encoding :: Wave_Fmt_16 -> Maybe Header.Encoding
wave_fmt_16_to_encoding fmt =
  lookup (audioFormat fmt, bitsPerSample fmt) wave_fmt_16_encoding_tbl

encoding_to_fmt_bps :: Header.Encoding -> Maybe (Wave_Format, Word16)
encoding_to_fmt_bps enc = Riff.reverse_lookup enc wave_fmt_16_encoding_tbl

encoding_to_fmt_bps_err :: Header.Encoding -> (Wave_Format, Word16)
encoding_to_fmt_bps_err = fromMaybe (error "encoding_to_fmt_bps?") . encoding_to_fmt_bps

-- | Erroring variant.
wave_fmt_16_encoding_err :: Wave_Fmt_16 -> Header.Encoding
wave_fmt_16_encoding_err = fromMaybe (error "wave_fmt_16_encoding") . wave_fmt_16_to_encoding

wave_fmt_16_parse :: Riff.Chunk -> Wave_Fmt_16
wave_fmt_16_parse ((nm, sz), dat) =
  if nm /= "fmt " || sz < 16
    then error "wave_fmt_16_parse?"
    else
      Wave_Fmt_16
        (Convert.word16_to_enum (Byte.decode_word16_le (Riff.section_int64 0 2 dat)))
        (Byte.decode_word16_le (Riff.section_int64 2 2 dat)) -- numChannels
        (Byte.decode_word32_le (Riff.section_int64 4 4 dat)) -- sampleRate
        (Byte.decode_word32_le (Riff.section_int64 8 4 dat)) -- byteRate
        (Byte.decode_word16_le (Riff.section_int64 12 2 dat)) -- blockAlign
        (Byte.decode_word16_le (Riff.section_int64 14 2 dat)) -- bitsPerSample
        (if sz > 16 then Byte.decode_word16_le (Riff.section_int64 16 2 dat) else 0)

{- | "fmt " encode

>>> let fmt = Wave_Fmt_16 Wave_Format_Ieee_Float 1 48000 (48000 * 4) 4 32 0
>>> let enc = [3, 0, 1, 0, 0x80, 0xBB, 0, 0, 0, 0xEE, 2, 0, 4, 0, 0x20, 0]
>>> ByteString.unpack (wave_fmt_16_encode fmt) == enc
True
-}
wave_fmt_16_encode :: Wave_Fmt_16 -> ByteString.ByteString
wave_fmt_16_encode hdr =
  ByteString.concat
    [ Byte.encode_word16_le (Convert.enum_to_word16 (audioFormat hdr))
    , Byte.encode_word16_le (numChannels hdr)
    , Byte.encode_word32_le (sampleRate hdr)
    , Byte.encode_word32_le (byteRate hdr)
    , Byte.encode_word16_le (blockAlign hdr)
    , Byte.encode_word16_le (bitsPerSample hdr)
    ]

wave_load_ch :: FilePath -> IO [Riff.Chunk]
wave_load_ch fn = withFile fn ReadMode wave_read_ch

-- | (Wave-Hdr,N-Frames,Dat-Chk,Other-Chks)
wave_load_ll :: FilePath -> IO (Wave_Fmt_16, Word32, Riff.Chunk, [Riff.Chunk])
wave_load_ll fn = do
  ch <- wave_load_ch fn
  let hdr = wave_fmt_16_parse (Riff.find_chunk_err "fmt " ch)
      dat = Riff.find_chunk_err "data" ch
      ((_, sz), _) = dat
      ch' = Riff.filter_chunks (/= "data") ch
  return (hdr, sz `div` Convert.word16_to_word32 (blockAlign hdr), dat, ch')

wave_fmt_16_to_header :: (Wave_Fmt_16, Word32) -> Header.Sf_Header
wave_fmt_16_to_header (fmt, nf) =
  let enc = wave_fmt_16_encoding_err fmt
  in Header.Sf_Header
      (Convert.word32_to_int nf)
      enc
      (Convert.word32_to_int (sampleRate fmt))
      (Convert.word16_to_int (numChannels fmt))

wave_load_hl :: FilePath -> IO (Header.Sf_Header, ByteString.ByteString)
wave_load_hl fn = do
  (fmt, nf, (_, dat), _) <- wave_load_ll fn
  let hdr = wave_fmt_16_to_header (fmt, nf)
  return (hdr, dat)

wave_load :: (Real n, Floating n) => FilePath -> IO (Header.Sf_Header, [[n]])
wave_load fn = do
  (hdr, bs) <- wave_load_hl fn
  return (hdr, Decode.decode_le (Header.encoding hdr) (Header.channelCount hdr) bs)

{- | Load 32-bit Float file to channel Vectors.

>>> fn = "/home/rohan/sw/hsc3-sf/data/wave/mc-4-16.wave"
>>> (hdr,vec) <- wave_load_vec_f32 fn
>>> hdr
Sf_Header {frameCount = 4, encoding = Float, sampleRate = 1, channelCount = 16}
-}
wave_load_vec_f32 :: FilePath -> IO (Header.Sf_Header, [Vector.Vector Float])
wave_load_vec_f32 fn = do
  (hdr, bs) <- wave_load_hl fn
  let nc = Header.channelCount hdr
      vec = Vector.vec_generate_f32_le (Header.frameCount hdr * nc) bs
  when (Header.encoding hdr /= Header.Float) (error "wave_load_vec_f32: not f32")
  return (hdr, Vector.vec_deinterleave nc vec)

-- | Write Wave file given header and encoded data.
wave_store_enc :: FilePath -> Header.Sf_Header -> ByteString.ByteString -> IO ()
wave_store_enc fn (Header.Sf_Header nf enc sr nc) d =
  let (fmt, bps) = encoding_to_fmt_bps_err enc
      ws = fromIntegral bps `div` 8
      hdr =
        Wave_Fmt_16
          fmt
          (Convert.int_to_word16 nc)
          (Convert.int_to_word32 sr)
          (Convert.int_to_word32 (sr * ws * nc))
          (Convert.int_to_word16 (ws * nc))
          bps
          0
      hdr_ch = (("fmt ", 16), wave_fmt_16_encode hdr)
      dat_ch = (("data", Convert.int_to_word32 (nf * nc * ws)), d)
      wr_f h = wave_write_ch h [hdr_ch, wave_fact_ch (Convert.int_to_word32 nf), dat_ch]
  in withFile fn WriteMode wr_f

-- | Write Int16 Wave file.
wave_store_i16 :: FilePath -> Header.Sf_Header -> [[Int16]] -> IO ()
wave_store_i16 fn hdr d =
  if Header.encoding hdr /= Header.Linear16
    then error "wave_store_i16: not I16?"
    else wave_store_enc fn hdr (Encode.encode_i16_le d)

-- | Write F32 Wave file.
wave_store_f32 :: (Real n, Floating n) => FilePath -> Header.Sf_Header -> [[n]] -> IO ()
wave_store_f32 fn hdr d =
  if Header.encoding hdr /= Header.Float
    then error "wave_store_f32: not Float"
    else wave_store_enc fn hdr (Encode.encode_le Header.Float d)

-- * Smpl

-- | Smpl Header (9 * 4 = 36-BYTES)
type Smpl_Hdr = [Word32]

-- | Smpl_Hdr field names.
smpl_hdr_fld :: [String]
smpl_hdr_fld =
  [ "MANUFACTURER-ID"
  , "PRODUCT-ID"
  , "SAMPLE-PERIOD"
  , "MIDI-NOTE"
  , "MIDI-DETUNE"
  , "SMPTE-FORMAT"
  , "SMPTE-OFFSET"
  , "SAMPLE-LOOPS"
  , "SAMPLER-DATA"
  ]

smpl_hdr_pp :: [Word32] -> String
smpl_hdr_pp = intercalate "," . zipWith (\nm x -> nm ++ "=" ++ show x) smpl_hdr_fld

{- | Smpl Lp (6 * 4 = 24-BYTES)
  Start and end indices are in bytes not frames.
-}
type Smpl_Lp = [Word32]

-- | Smpl_Lp field names.
smpl_lp_fld :: [String]
smpl_lp_fld = words "CUE-ID TYPE START END FRACTION PLAY-COUNT"

smpl_lp_pp :: [Word32] -> String
smpl_lp_pp = intercalate "," . zipWith (\nm x -> nm ++ "=" ++ show x) smpl_lp_fld

-- | If the sample-loops field is 0 (Akai) still get Smpl_Lp
wave_smpl_parse :: Riff.Chunk -> (Smpl_Hdr, [Smpl_Lp], ByteString.ByteString)
wave_smpl_parse (hdr, dat) =
  case hdr of
    ("smpl", sz) ->
      let f n = Byte.decode_word32_le (Riff.section_word32 n 4 dat)
          g k = [f k, f (k + 4), f (k + 8), f (k + 12), f (k + 16), f (k + 20)]
          h k = g (k * 24 + 36)
          lp_n = f 28
          dat_n = f 32
          lp_n_c = (sz - 9 * 4 - dat_n) `div` 24 -- LOOP-N (CALCULATED)
      in ( [f 0, f 4, f 8, f 12, f 16, f 20, f 24, lp_n, dat_n]
         , map h [0 .. lp_n_c - 1]
         , Riff.section_word32 ((9 * 4) + (lp_n_c * 24)) dat_n dat
         )
    _ -> error "wave_smpl_parse?"

smpl_pp :: (Smpl_Hdr, [Smpl_Lp], ByteString.ByteString) -> [String]
smpl_pp (hdr, lp, _) = smpl_hdr_pp hdr : map smpl_lp_pp lp

-- * Cue

-- | Cue_Dat (6 * 4 = 24-BYTES)
type Cue_Dat = [Word32]

cue_dat_fld :: [String]
cue_dat_fld = words "ID POSITION DATA-CHUNK-ID CHUNK-START BLOCK-START SAMPLE-OFFSET"

cue_dat_pp :: [Word32] -> [String]
cue_dat_pp =
  let pp_f = [show, show, reverse . Riff.word32_to_ascii, show, show, show]
  in zipWith3 (\nm pp x -> nm ++ "=" ++ pp x) cue_dat_fld pp_f

wave_cue_parse :: Riff.Chunk -> (Word32, [Cue_Dat])
wave_cue_parse (hdr, dat) =
  case hdr of
    ("cue ", _sz) ->
      let f n = Byte.decode_word32_le (Riff.section_word32 n 4 dat)
          n_dat = f 0
          g k = [f k, f (k + 4), f (k + 8), f (k + 12), f (k + 16), f (k + 20)]
      in (n_dat, map g (genericTake n_dat [4, 28 ..]))
    _ -> error "wave_cue_parse?"

cue_pp :: (Word32, [Cue_Dat]) -> [String]
cue_pp (n, dat) = ("N-CUE=" ++ show n) : map (intercalate "," . cue_dat_pp) dat

{-

fn = "/home/rohan/SYN/EMU/UNIVERSE OF SOUNDS FAVOURITES/ARIEL PAD/S50_AR C2.wav"
fn = "/home/rohan/data/audio/instr/casacota/zell_1737_415_MeanTone5/8_i/8_i_01.wav"
fn = "/home/rohan/data/audio/instr/casacota/Hauslaib_1590_415_MeanTone4/flauteadillos/25-c3.wav"
ch <- wave_load_ch fn
map fst ch
wave_fmt_16_parse (find_chunk_err "fmt " ch)
wave_fact_parse (find_chunk_err "fact" ch)
putStrLn $ unlines $ smpl_pp $ wave_smpl_parse (find_chunk_err "smpl" ch)
putStrLn $ unlines $ cue_pp $ wave_cue_parse (find_chunk_err "cue " ch)

(hdr,dat) <- wave_load fn
hdr
length dat == Header.channelCount hdr
map length dat == replicate (Header.channelCount hdr) (Header.frameCount hdr)

wave_store_f32 "/tmp/t.wav" (hdr {Header.encoding = Header.Float}) dat

import Sound.SC3.Plot
plotTable (map (take (2 ^ 12)) dat)

wave_store_f32 "/tmp/t.wav" (Header.Sf_Header 24000 Header.Float 48000 1) [take 24000 (cycle [0,1])]

-}
