-- | Decoder for audio data.
module Sound.File.Decode where

import Data.Int {- base -}
import Data.List {- base -}

import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.List.Split as Split {- split -}

import qualified Sound.Osc.Coding.Byte as Coding.Byte {- hosc -}

import qualified Sound.File.Header as Header {- hsc3-sf -}

{- | Given channel count, deinterleave list to set of channels.

>>> deinterleave 2 [0..9]
[[0,2,4,6,8],[1,3,5,7,9]]
-}
deinterleave :: Int -> [a] -> [[a]]
deinterleave nc = transpose . Split.chunksOf nc

{- | 'ByteString.ByteString' variant of 'Split.chunksOf'.

>>> Split.chunksOf 4 ['a' .. 'p']
["abcd","efgh","ijkl","mnop"]

>>> bSetsOf 4 (ByteString.pack [97 .. 112])
["abcd","efgh","ijkl","mnop"]
-}
bSetsOf :: Int64 -> ByteString.ByteString -> [ByteString.ByteString]
bSetsOf n b =
  if ByteString.null b
    then []
    else
      let (x, y) = ByteString.splitAt n b
      in x : bSetsOf n y

decode_fp :: (Fractional f, Real r) => (ByteString.ByteString -> r) -> Int64 -> Int -> ByteString.ByteString -> [[f]]
decode_fp f sz nc = deinterleave nc . map (realToFrac . f) . bSetsOf sz

decode_f32 :: Floating n => Int -> ByteString.ByteString -> [[n]]
decode_f32 = decode_fp Coding.Byte.decode_f32 4

decode_f32_le :: Floating n => Int -> ByteString.ByteString -> [[n]]
decode_f32_le = decode_fp Coding.Byte.decode_f32_le 4

decode_f64 :: Floating n => Int -> ByteString.ByteString -> [[n]]
decode_f64 = decode_fp Coding.Byte.decode_f64 8

decode_i :: (ByteString.ByteString -> i) -> Int64 -> Int -> ByteString.ByteString -> [[i]]
decode_i f sz nc = deinterleave nc . map f . bSetsOf sz

decode_i32 :: Int -> ByteString.ByteString -> [[Int]]
decode_i32 = decode_i Coding.Byte.decode_i32 4

decode_i16 :: Int -> ByteString.ByteString -> [[Int]]
decode_i16 = decode_i Coding.Byte.decode_i16 2

decode_i16_le :: Int -> ByteString.ByteString -> [[Int]]
decode_i16_le = decode_i Coding.Byte.decode_i16_le 2

decode_i8 :: Int -> ByteString.ByteString -> [[Int]]
decode_i8 = decode_i Coding.Byte.decode_i8 1

i8_f :: (Real n, Floating n) => [Int] -> [n]
i8_f = map ((/ 128.0) . fromIntegral)

i16_f :: (Real n, Floating n) => [Int] -> [n]
i16_f = map ((/ 32768.0) . fromIntegral)

i32_f :: (Real n, Floating n) => [Int] -> [n]
i32_f = map ((/ 2147483648.0) . fromIntegral)

{- | Given an 'Encoding' and the number of channels, decode
a 'ByteString.ByteString' to set of 'deinterleave'd channels.
-}
decode :: (Real n, Floating n) => Header.Encoding -> Int -> ByteString.ByteString -> [[n]]
decode enc nc b =
  case enc of
    Header.Linear8 -> map i8_f (decode_i8 nc b)
    Header.Linear16 -> map i16_f (decode_i16 nc b)
    Header.Linear32 -> map i32_f (decode_i32 nc b)
    Header.Float -> decode_f32 nc b
    Header.Double -> decode_f64 nc b

decode_le :: (Real n, Floating n) => Header.Encoding -> Int -> ByteString.ByteString -> [[n]]
decode_le enc nc b =
  case enc of
    Header.Linear16 -> map i16_f (decode_i16_le nc b)
    Header.Float -> decode_f32_le nc b
    _ -> error "decode_le: not implemented"
