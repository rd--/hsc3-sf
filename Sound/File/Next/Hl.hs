-- | Read and write Next/Sun format sound files.
module Sound.File.Next.Hl where

import Control.Monad {- base -}
import Data.Int {- base -}
import Data.Maybe {- base -}
import System.IO {- base -}

import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Sound.File.Decode as Decode
import qualified Sound.File.Encode as Encode
import qualified Sound.File.Header as Header
import qualified Sound.File.Next.Ll as Next.Ll
import qualified Sound.File.Vector as U

-- | Index into 'ByteString.Bytestring'.
type Offset = Int64

-- | 32-byte Next header (no annotations).
header_to_ll :: Header.Sf_Header -> Next.Ll.Next_Header_Ll
header_to_ll (Header.Sf_Header nf enc sr nc) =
  let nf' = fromIntegral nf
      sr' = fromIntegral sr
      nc' = fromIntegral nc
      enc' = Next.Ll.encoding_to_au enc
      nb = nf' * nc' * Header.encoding_bytes enc
  in (Next.Ll.next_magic, 32, nb, enc', sr', nc', replicate 8 0)

header_from_next_header :: Next.Ll.Next_Header -> Header.Sf_Header
header_from_next_header (m, _off, sz, enc, sr, nc, _ann) =
  if m /= ".snd"
    then error "read_header: not .snd"
    else
      let enc' = fromMaybe (error "read_header: unknown encoding") enc
          nf = sz `div` (nc * Header.encoding_bytes enc')
      in Header.Sf_Header nf enc' sr nc

header_from_ll :: (Next.Ll.Next_Header_Ll, Int) -> Header.Sf_Header
header_from_ll = header_from_next_header . Next.Ll.next_header_from_ll

-- | Byte-encode a Next header.
encode_header :: Header.Sf_Header -> ByteString.ByteString
encode_header = Next.Ll.next_header_ll_encode . header_to_ll

au_write_header :: Handle -> Header.Sf_Header -> IO ()
au_write_header h = ByteString.hPut h . encode_header

-- | Read header (or not) and leave handle at start of data segment (or not).
au_read_header :: Handle -> IO (Header.Sf_Header, Int)
au_read_header h = do
  hdr_ll <- Next.Ll.read_next_header_ll h
  actual_sz <- Next.Ll.header_ll_actual_data_size hdr_ll h
  let (m, off, sz, enc, sr, nc, _ann) = Next.Ll.next_header_from_ll (hdr_ll, actual_sz)
      enc' = fromMaybe (error "read_header: unknown encoding") enc
  when (m /= ".snd") (error "read_header: not .snd")
  let nf = sz `div` (nc * Header.encoding_bytes enc')
  return (Header.Sf_Header nf enc' sr nc, off)

-- | Given handle at start of data segment, read & decode data.
au_read_data :: (Real n, Floating n) => Header.Sf_Header -> Handle -> IO [[n]]
au_read_data (Header.Sf_Header nf enc _ nc) h = do
  let sz = nf * nc * Header.encoding_bytes enc
  b <- ByteString.hGet h sz
  return (Decode.decode enc nc b)

{- | Read sound file meta data.

>>> au_header "/home/rohan/sw/hsc3-sf/data/au/mc-11-2048.au"
Sf_Header {frameCount = 11, encoding = Float, sampleRate = 1, channelCount = 2048}
-}
au_header :: FilePath -> IO Header.Sf_Header
au_header fn = withFile fn ReadMode (fmap fst . au_read_header)

-- | Write pre-encoded data to file.
au_write_b :: FilePath -> Header.Sf_Header -> ByteString.ByteString -> IO ()
au_write_b fn (Header.Sf_Header nf enc sr nc) d =
  let h = encode_header (Header.Sf_Header nf enc sr nc)
      b = ByteString.append h d
  in ByteString.writeFile fn b

-- | Write sound file, data is non-interleaved.
au_write :: Real n => FilePath -> Header.Sf_Header -> [[n]] -> IO ()
au_write fn (Header.Sf_Header nf enc sr nc) d =
  let b = Encode.encode enc d
  in au_write_b fn (Header.Sf_Header nf enc sr nc) b

{- | Read Next file.  The 'header_ll_actual_data_size' is read.

>>> (hdr, dat) <- au_read_b "/home/rohan/sw/hsc3-sf/data/au/mc-4-16.au"
>>> hdr
Sf_Header {frameCount = 4, encoding = Float, sampleRate = 1, channelCount = 16}

>>> ByteString.length dat
256
-}
au_read_b :: FilePath -> IO (Header.Sf_Header, ByteString.ByteString)
au_read_b fn = do
  h <- openFile fn ReadMode
  hdr_ll <- Next.Ll.read_next_header_ll h
  sz <- Next.Ll.header_ll_actual_data_size hdr_ll h
  let hdr = header_from_ll (hdr_ll, sz)
  b <- ByteString.hGet h sz
  hClose h
  return (hdr, b)

-- | Read sound file, data is interleaved.
au_read :: (Real n, Floating n) => FilePath -> IO (Header.Sf_Header, [[n]])
au_read fn = do
  (Header.Sf_Header nf enc sr nc, b) <- au_read_b fn
  let d = Decode.decode enc nc b
  return (Header.Sf_Header nf enc sr nc, d)

-- * Vector

au_read_data_f32_vec :: Header.Sf_Header -> Handle -> IO (Vector.Vector Float)
au_read_data_f32_vec hdr h = do
  when (Header.encoding hdr /= Header.Float) (error "au_read_data_f32_vec: not Float data")
  let ne = Header.frameCount hdr * Header.channelCount hdr
  U.vec_read_f32 h ne

{- | Read a 32 bit floating point Next sound file.

>>> (hdr,vec) <- au_read_f32_vec "/home/rohan/sw/hsc3-sf/data/au/mc-4-16.au"
>>> hdr
Sf_Header {frameCount = 4, encoding = Float, sampleRate = 1, channelCount = 16}

>>> Vector.length vec
64
-}
au_read_f32_vec :: FilePath -> IO (Header.Sf_Header, Vector.Vector Float)
au_read_f32_vec fn = do
  h <- openFile fn ReadMode
  (hdr, _) <- au_read_header h
  vec <- au_read_data_f32_vec hdr h
  hClose h
  return (hdr, vec)

{- | Write a 32 bit floating point Next sound file.

>>> (hdr,vec) <- au_read_f32_vec "/home/rohan/sw/hsc3-sf/data/au/mc-4-16.au"
>>> au_write_f32_vec "/tmp/mc-4-16.au" (hdr,vec)
>>> (hdr',vec') <- au_read_f32_vec "/tmp/mc-4-16.au"
>>> hdr == hdr' && vec == vec'
True
-}
au_write_f32_vec :: FilePath -> (Header.Sf_Header, Vector.Vector Float) -> IO ()
au_write_f32_vec fn (hdr, vec) = do
  h <- openFile fn WriteMode
  au_write_header h hdr
  let n = Header.frameCount hdr * Header.channelCount hdr
  U.vec_write_f32 h n vec
  hClose h
