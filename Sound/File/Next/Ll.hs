-- | Low-level
module Sound.File.Next.Ll where

import Data.Maybe {- base -}
import Data.Word {- base -}
import System.IO {- base -}

import qualified Data.Binary as Binary {- binary -}
import qualified Data.ByteString.Lazy as ByteString {- bytestring -}

import qualified Sound.File.Header as Header

{- | Next code to 'Encoding'.

>>> import Data.Word
>>> au_to_encoding_maybe (6::Word32)
Just Float
-}
au_to_encoding_maybe :: (Eq n, Num n) => n -> Maybe Header.Encoding
au_to_encoding_maybe n =
  case n of
    2 -> Just Header.Linear8
    3 -> Just Header.Linear16
    -- 4 -> Just Header.Linear24
    5 -> Just Header.Linear32
    6 -> Just Header.Float
    7 -> Just Header.Double
    _ -> Nothing

-- | Next code to 'Encoding', or 'error'.
au_to_encoding :: (Eq n, Num n) => n -> Header.Encoding
au_to_encoding = fromMaybe (error "toEncoding: unknown encoding") . au_to_encoding_maybe

{- | Next code of 'Encoding'

>>> encoding_to_au Header.Float
6
-}
encoding_to_au :: Num n => Header.Encoding -> n
encoding_to_au c =
  case c of
    Header.Linear8 -> 2
    Header.Linear16 -> 3
    Header.Linear32 -> 5
    Header.Float -> 6
    Header.Double -> 7

read_u32 :: Handle -> IO Word32
read_u32 h = fmap Binary.decode (ByteString.hGet h 4)

word32_bytes :: Word32 -> [Word8]
word32_bytes = ByteString.unpack . Binary.encode

word8_to_char :: Word8 -> Char
word8_to_char = toEnum . fromIntegral

{- | Magic to string

>>> magic_to_string next_magic
".snd"
-}
magic_to_string :: Word32 -> String
magic_to_string = map word8_to_char . word32_bytes

next_header_fields :: [String]
next_header_fields =
  [ "magic"
  , "data-offset/bytes"
  , "data-size/bytes"
  , "data-encoding"
  , "sample-rate"
  , "channels"
  , "annotation"
  ]

type Next_Header_Ll = (Word32, Word32, Word32, Word32, Word32, Word32, [Word8])
type Next_Header = (String, Int, Int, Maybe Header.Encoding, Int, Int, String)

-- | The Next header magic number.
next_magic :: Num n => n
next_magic = 0x2e736e64

-- | The value of the /data-size/ field that indicates unknown extent.
unknown_data_size :: Num n => n
unknown_data_size = 0xffffffff

read_next_header_ll :: Handle -> IO Next_Header_Ll
read_next_header_ll h = do
  magic <- read_u32 h
  data_offset <- read_u32 h
  data_size <- read_u32 h
  data_encoding <- read_u32 h
  sample_rate <- read_u32 h
  channels <- read_u32 h
  annotation <- fmap ByteString.unpack (ByteString.hGet h (fromIntegral data_offset - 24))
  return (magic, data_offset, data_size, data_encoding, sample_rate, channels, annotation)

load_next_header_ll :: FilePath -> IO Next_Header_Ll
load_next_header_ll fn = withFile fn ReadMode read_next_header_ll

header_ll_actual_data_size :: Next_Header_Ll -> Handle -> IO Int
header_ll_actual_data_size (_, off, sz, _, _, _, _) h =
  if sz == unknown_data_size
    then fmap (\n -> fromIntegral n - fromIntegral off) (hFileSize h)
    else return (fromIntegral sz)

next_header_ll_encode :: Next_Header_Ll -> ByteString.ByteString
next_header_ll_encode (m, off, sz, enc, sr, ch, ann) =
  let f = Binary.encode
  in ByteString.concat [f m, f off, f sz, f enc, f sr, f ch, ByteString.pack ann]

next_header_from_ll :: (Next_Header_Ll, Int) -> Next_Header
next_header_from_ll ((m, off, sz, enc, sr, ch, ann), actual_sz) =
  ( magic_to_string m
  , fromIntegral off
  , if sz == unknown_data_size then actual_sz else fromIntegral sz
  , au_to_encoding_maybe enc
  , fromIntegral sr
  , fromIntegral ch
  , map word8_to_char ann
  )

next_header_pp :: Next_Header -> [String]
next_header_pp (m, off, sz, enc, sr, ch, ann) =
  let f k v = concat [k, " = ", v]
      sz' = if sz == unknown_data_size then "Unknown-Data-Size" else show sz
      enc' = maybe "Unknown-Encoding" show enc
  in zipWith f next_header_fields [m, show off, sz', enc', show sr, show ch, ann]
