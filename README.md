hsc3-sf
-------

Simple sound file input/output.

[hsc3-sf](http://rohandrape.net/?t=hsc3-sf)
provides `Sound.File.Next` and `Sound.File.Wave`,
[Haskell](http://haskell.org/)
modules that provide sound file input/output for
NeXT/Sun and WAVE format sound files.

For more comprehensive sound file support in haskell see
[hsndfile](http://hackage.haskell.org/package/hsndfile).

## Cli

[au-print](?t=hsc3-sf&e=md/au-print.md),
[id3](?t=hsc3-sf&e=md/id3.md),
[sf](?t=hsc3-sf&e=md/sf.md)

© [Rohan Drape](http://rohandrape.net/), 2006-2024, [Gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 40  Tried: 40  Errors: 0  Failures: 0
$
```
