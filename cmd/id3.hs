{-

COMM ©cmt COMMENT comment
TALB ©alb ALBUM album
TCOM ©wrt COMPOSER composer
TCON ©gen GENRE genre
TDRC ©day YEAR year
TIT1 ©grp CONTENTGROUP grouping
TIT2 ©nam TITLE title
TPE1 ©art ARTIST artist
TPE2 aART ALBUMARTIST albumArtist
TPE3 ©con CONDUCTOR -
TPOS disc DISCNUMBER disk
TPUB -    PUBLISHER -
TRCK trkn TRACK tracknum
WCOM -    WWWCOMMERCIALINFO -

mid3v2 -- python3-mutagen

-}

import Control.Monad {- base -}
import Data.Maybe {- base -}
import System.Environment {- base -}
import System.Exit {- base -}
import System.Process {- process -}

import qualified Sound.File.Id3 as Id3 {- hsc3-sf -}

-- | (KEY,VALUE)
type TAG = (String, String)

type META = [TAG]

{- | Allow either of two forms.

> tag_parse "TDRC=2016-03-13" == ("TDRC","2016-03-13")
> tag_parse "TPUB: Alfa" == ("TPUB","Alfa")
-}
tag_parse :: String -> TAG
tag_parse s =
  case (break (== '=') s, break (== ':') s) of
    ((k, '=' : v), (_, "")) -> (k, v)
    ((_, ""), (k, ':' : ' ' : v)) -> (k, v)
    _ -> error "parse_tag"

tag_is_valid :: TAG -> Bool
tag_is_valid (k, _) = k `elem` map fst Id3.id3v2_4_tags

meta_verify :: META -> IO ()
meta_verify = mapM_ (\m -> unless (tag_is_valid m) (error (show ("meta_verify: failed", m))))

-- | Read initial non-empty lines from file and run 'tag_parse'
meta_load :: FilePath -> IO META
meta_load fn = do
  s <- readFile fn
  let m = map tag_parse (takeWhile (not . null) (lines s))
  meta_verify m
  return m

-- > tag_to_mid3v2_opt ("TDRC","2016-03-13") == ["--TDRC","2016-03-13"]
tag_to_mid3v2_opt :: TAG -> [String]
tag_to_mid3v2_opt (k, v) = ["--" ++ k, v]

id3_atomicparsley_tbl :: [(String, String)]
id3_atomicparsley_tbl =
  [ ("COMM", "comment")
  , ("TALB", "album")
  , ("TCOM", "composer")
  , ("TCON", "genre")
  , ("TDRC", "year")
  , ("TIT2", "title")
  , ("TPE1", "artist")
  , ("TPE2", "albumArtist")
  , ("TPOS", "disk")
  , ("TRCK", "tracknum")
  ]

tag_to_atomicparsley_opt :: TAG -> Maybe [String]
tag_to_atomicparsley_opt (k, v) =
  let f o = ["--" ++ o, v]
  in fmap f (lookup k id3_atomicparsley_tbl)

id3_metaflac_tbl :: [(String, String)]
id3_metaflac_tbl =
  [ ("COMM", "COMMENT")
  , ("TALB", "ALBUM")
  , ("TCOM", "COMPOSER")
  , ("TCON", "GENRE")
  , ("TDRC", "YEAR")
  , ("TIT2", "TITLE")
  , ("TPE1", "ARTIST")
  , ("TPE2", "ALBUMARTIST")
  , ("TPE3", "CONDUCTOR")
  , ("TPOS", "DISCNUMBER")
  , ("TPUB", "PUBLISHER")
  , ("TRCK", "TRACK")
  , ("WCOM", "WWWCOMMERCIALINFO")
  ]

-- > tag_to_metaflac_opt ("TDRC","2016") == Just ["--remove-tag=YEAR","--set-tag=YEAR=2016"]
tag_to_metaflac_opt :: TAG -> Maybe [String]
tag_to_metaflac_opt (k, v) =
  let f o = map concat [["--remove-tag=", o], ["--set-tag=", o, "=", v]]
  in fmap f (lookup k id3_metaflac_tbl)

type CMD = (String, [String])

cmd_to_str :: CMD -> String
cmd_to_str (c, a) = unwords (c : a)

run_cmd :: CMD -> IO ExitCode
run_cmd = uncurry rawSystem

run_cmd_err :: CMD -> IO ()
run_cmd_err c = do
  putStrLn (cmd_to_str c)
  err <- run_cmd c
  when (err /= ExitSuccess) (print ("run_cmd: ", c))

run_cmd_seq :: [CMD] -> IO ()
run_cmd_seq = mapM_ run_cmd_err

mp3_add_meta_cmd :: META -> [FilePath] -> [CMD]
mp3_add_meta_cmd m mp3_fn = [("mid3v2", concatMap tag_to_mid3v2_opt m ++ mp3_fn)]

-- > flac_add_meta_cmd [("TPE1","a b"),("TDRC","1234")] ["c d.flac"]
flac_add_meta_cmd :: META -> [FilePath] -> [CMD]
flac_add_meta_cmd m flac_fn = [("metaflac", concat (mapMaybe tag_to_metaflac_opt m) ++ flac_fn)]

m4a_add_meta_cmd :: META -> [FilePath] -> [CMD]
m4a_add_meta_cmd m =
  let f m4a_fn =
        ( "AtomicParsley"
        , m4a_fn : concat (mapMaybe tag_to_atomicparsley_opt m) ++ ["--overWrite"]
        )
  in map f

add_id3 :: (META -> [FilePath] -> [CMD]) -> FilePath -> [FilePath] -> IO ()
add_id3 cmd_f meta_fn fn = do
  m <- meta_load meta_fn
  run_cmd_seq (cmd_f m fn)

main :: IO ()
main = do
  a <- getArgs
  case a of
    "mp3" : "add-id3" : meta_fn : fn -> add_id3 mp3_add_meta_cmd meta_fn fn
    "m4a" : "add-id3" : meta_fn : fn -> add_id3 m4a_add_meta_cmd meta_fn fn
    "flac" : "add-id3" : meta_fn : fn -> add_id3 flac_add_meta_cmd meta_fn fn
    _ -> putStrLn "hsc3-id3 mp3|m4a|flac add-id3 id3-file sound-file..."
