import Control.Monad {- base -}
import Data.Bifunctor {- base -}
import Data.Bits {- base -}
import Data.List {- base -}
import Data.Word {- base -}
import System.Environment {- base -}
import System.IO {- base -}

import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Sound.File.Next as Next {- hsc3-sf -}
import qualified Sound.File.Riff as Riff {- hsc3-sf -}
import qualified Sound.File.Vector as Vector {- hsc3-sf -}
import qualified Sound.File.Wave as Wave {- hsc3-sf -}

-- * Au/Header

au_header_hl :: FilePath -> IO ()
au_header_hl fn = do
  hdr <- Next.au_header fn
  print hdr

au_header_ll :: FilePath -> IO ()
au_header_ll fn = do
  hdr_ll <- Next.load_next_header_ll fn
  let hdr = Next.next_header_from_ll (hdr_ll, Next.unknown_data_size)
  mapM_ putStrLn $ Next.next_header_pp hdr

-- * Au/Rescale

au_rescale :: Double -> FilePath -> FilePath -> IO ()
au_rescale n in_fn out_fn = do
  (hdr, d) <- Next.au_read in_fn
  let d' = map (map (* n)) d
      hdr' = hdr {Next.encoding = Next.Float}
  Next.au_write out_fn hdr' d'

-- * Au/Stat

type STAT = [(String, String)]

au_stat_f32_vec :: Next.Sf_Header -> Handle -> IO STAT
au_stat_f32_vec hdr h = do
  vec <- Next.au_read_data_f32_vec hdr h
  let vec' = Vector.vec_deinterleave (Next.channelCount hdr) vec
  return
    [ ("minima", show (map (Vector.foldl1 min) vec'))
    , ("maxima", show (map (Vector.foldl1 max) vec'))
    ]

au_stat_plain :: Next.Sf_Header -> Handle -> IO STAT
au_stat_plain hdr h = do
  d <- Next.au_read_data hdr h :: IO [[Double]]
  return
    [ ("minima", show (map minimum d))
    , ("maxima", show (map maximum d))
    ]

au_stat :: (Next.Sf_Header -> Handle -> IO STAT) -> FilePath -> IO ()
au_stat stat_f fn = do
  h <- openFile fn ReadMode
  sz <- hFileSize h
  (hdr, off) <- Next.au_read_header h
  stat <- stat_f hdr h
  hClose h
  let Next.Sf_Header nf enc sr nc = hdr
      sz' = fromInteger sz - off
      tbl =
        [ ("file-size/bytes", show sz)
        , ("data-offset/bytes", show off)
        , ("encoding", show enc)
        , ("encoding-size/bytes", show (Next.encoding_bytes enc :: Int))
        , ("frame-count/header", show nf)
        , ("frame-count/data", show (sz' `div` (nc * Next.encoding_bytes enc)))
        , ("sample-rate", show sr)
        , ("channel-count", show nc)
        , ("duration/seconds", show (Next.sf_header_duration hdr :: Double))
        ]
  mapM_ (putStrLn . (\(k, v) -> k ++ " = " ++ v)) (tbl ++ stat)

-- * Multi-Channel

mc_gen_fn :: FilePath -> String -> Int -> Int -> FilePath
mc_gen_fn dir ty n m = concat [dir, "/mc-", show n, "-", show m, ".", ty]

mc_gen_dat :: Int -> Int -> [[Double]]
mc_gen_dat nf nc =
  let gen_ch n = map (fromIntegral . fromEnum . testBit n) [0 .. nf - 1]
  in map gen_ch [0 .. nc - 1]

type MC_GEN_F = FilePath -> Next.Sf_Header -> [[Double]] -> IO ()

mc_gen :: MC_GEN_F -> FilePath -> Next.FrameCount -> Next.ChannelCount -> IO ()
mc_gen f fn nf nc = do
  let hdr = Next.Sf_Header nf Next.Float 1 nc
  f fn hdr (mc_gen_dat nf nc)

mc_gen_set :: (MC_GEN_F, String) -> FilePath -> [Int] -> IO ()
mc_gen_set (f, ty) dir =
  let g n = let m = 2 ^ n in mc_gen f (mc_gen_fn dir ty n m) n m
  in mapM_ g

-- * Riff

sub_chunks :: Riff.Chunk -> Maybe (String, [Riff.Chunk])
sub_chunks ch =
  case ch of
    (("LIST", sz), dat) ->
      let fm = Riff.riff_parse_type (Riff.section_int64 0 4 dat)
      in Just (fm, Riff.riff_parse_chunk_seq (Riff.section_word32 4 (sz - 4) dat))
    _ -> Nothing

type Riff_Ch = (Word32, String, [(Riff.Chunk, Maybe (String, [Riff.Chunk]))])

riff_read_ch :: Handle -> IO Riff_Ch
riff_read_ch h = do
  (ty, sz) <- Riff.riff_read_chunk_hdr h
  when (ty /= "RIFF") (error "riff_read_ch: not Riff")
  fm <- Riff.read_word32_ascii h
  ch <- Riff.riff_read_chunk_seq h
  return (sz, fm, zip ch (map sub_chunks ch))

riff_load_ch :: FilePath -> IO Riff_Ch
riff_load_ch fn = withFile fn ReadMode riff_read_ch

riff_print_ch :: FilePath -> IO ()
riff_print_ch fn = do
  (sz, fm, dat) <- riff_load_ch fn
  print (sz, fm, map (bimap fst (fmap (second (map fst)))) dat)

-- * Wave

wave_header_ll :: FilePath -> IO ()
wave_header_ll fn = do
  (fmt, nf, dat, ch) <- Wave.wave_load_ll fn
  putStrLn (Wave.wave_fmt_16_pp fmt)
  putStrLn ("numFrames = " ++ show nf)
  putStrLn (intercalate "," (map (Riff.chunk_hdr_pp . fst) (dat : ch)))

wave_header_hl :: FilePath -> IO ()
wave_header_hl fn = Wave.wave_load_hl fn >>= print . fst

-- * Main

usage :: [String]
usage =
  [ "hsc3-sf"
  , ""
  , "  au header {ll | hl} file-name"
  , "  au rescale {db|lin} scl:real input-file output-file"
  , "  au stat {plain | f32-vec} file-name"
  , "  multi-channel file-type directory n:int m:int"
  , "  riff print-ch file-name..."
  , "  wave header {ll | hl} file-name"
  , ""
  , "    scl = gain as either decibel shift or linear multiplier"
  ]

main :: IO ()
main = do
  let db_to_amp = (10 **) . (* 0.05)
  a <- getArgs
  case a of
    ["au", "header", "ll", fn] -> au_header_ll fn
    ["au", "header", "hl", fn] -> au_header_hl fn
    ["au", "rescale", "db", g, in_fn, out_fn] -> au_rescale (db_to_amp (read g)) in_fn out_fn
    ["au", "rescale", "lin", g, in_fn, out_fn] -> au_rescale (read g) in_fn out_fn
    ["au", "stat", "plain", fn] -> au_stat au_stat_plain fn
    ["au", "stat", "f32-vec", fn] -> au_stat au_stat_f32_vec fn
    ["multi-channel", "au", dir, n, m] -> mc_gen_set (Next.au_write, "au") dir [read n .. read m]
    ["multi-channel", "wave", dir, n, m] -> mc_gen_set (Wave.wave_store_f32, "wave") dir [read n .. read m]
    "riff" : "print-ch" : fn -> mapM_ riff_print_ch fn
    ["wave", "header", "ll", fn] -> wave_header_ll fn
    ["wave", "header", "hl", fn] -> wave_header_hl fn
    _ -> putStrLn (unlines usage)

{-

au_header_ll "/home/rohan/rd/j/2019-04-21/FAIRLIGHT/IIX/PLUCKED/koto.snd"
au_stat au_stat_plain "/home/rohan/rd/j/2019-04-21/FAIRLIGHT/IIX/PLUCKED/koto.snd"

mc_gen_dat 3 8
import qualified Sound.File.HSndFile as Sf
let au_dir = "/home/rohan/sw/hsc3-sf/data/au/"
(_,d) <- Sf.read (au_dir ++ "/mc-6-64.au")
d == mc_gen_dat 6 64

fn = "/home/rohan/sw/hsc3-data/data/akai/S5000/ARIEL/S50_AR C2.wav"
fn = "/home/rohan/sw/hsc3-data/data/akai/S5000/ARIEL/S50_ARIEL.akp"
riff_print_ch fn

-}
