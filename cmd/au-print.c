#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "r-common/c/failure.h"
#include "r-common/c/img-png.h"
#include "r-common/c/int.h"
#include "r-common/c/memory.h"
#include "r-common/c/sf-au.h"

i32 index_co(i32 nc, i32 ch, i32 fr)
{
    return fr * nc + ch;
}

i32 index_ro(i32 nf, i32 ch, i32 fr)
{
    return ch * nf + fr;
}

void pp(i32 ch, i32 fr, i32 ix, f32 * d, bool bw, bool vb)
{
    if (vb) {
        printf("(%d,%d,%d) ", ch, fr, ix);
    }
    if (bw) {
        printf("%s ", d[ix] > 0.5 ? "1" : "0");
    } else {
        printf("%f ", d[ix]);
    }
}

bool str_bool(char *s, char *t, char *f, int n)
{
    if (strncmp(s, t, n) == 0) {
        return true;
    } else if (strncmp(s, f, n) == 0) {
        return false;
    } else {
        die("str_bool: %s is not %s (true) or %s (false)\n", s, t, f);
    }
}

int main(int argc, char **argv)
{
    if (argc != 5) {
        puts("au-print {vb|cs} {ro|co} {gs|bw} au-file");
        puts("  vb = verbose, cs = concise");
        puts("  ro = row order, co = column order");
        puts("  gs = grey scale, bw = black & white");
        exit(1);
    }
    bool vb = str_bool(argv[1], "vb", "cs", 2);
    bool co = str_bool(argv[2], "co", "ro", 2);
    bool bw = str_bool(argv[3], "bw", "gs", 2);
    char *nm = argv[4];
    i32 nc, nf, sr;
    f32 *d = read_au_f32(nm, &nc, &nf, &sr);
    printf("nm=\"%s\", nc=%d, nf=%d, sr=%d, co=%s\n", nm, nc, nf, sr, co ? "t" : "f");
    if (co) {
        for (i32 i = 0; i < nf; i++) {
            if (vb) {
                printf("C%d: ", i);
            }
            for (i32 j = 0; j < nc; j++) {
                pp(i, j, index_co(nc, j, i), d, bw, vb);
            }
            printf("\n");
        }
    } else {
        for (i32 i = 0; i < nc; i++) {
            if (vb) {
                printf("R%d: ", i);
            }
            for (i32 j = 0; j < nf; j++) {
                pp(i, j, index_co(nc, i, j), d, bw, vb);
            }
            printf("\n");
        }
    }
    free(d);
    return 0;
}
