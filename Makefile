all:
	echo "hsc3-sf"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hsc3-sf

push-tags:
	r.gitlab-push.sh hsc3-sf --tags

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
